#include <stdio.h>
int main()
{
    int num,a,b;
    a=0;
    printf("Enter your number : ");
    scanf("%d", &num);

    do
    {
        b = num%10;
        a = a*10+b;
        num = num/10;
    }
    while (num!=0);

    printf("Reversed number is %d", a);
    return 0;
}
