#include <stdio.h>
int main()
{
    int num,x,ans;
    x=1;
    printf("Enter your number : ");
    scanf("%d", &num);

    do
    {
        printf("%d * %d = %d \n", num, x, num*x);
        ans = num*x;
        x = x+1;
    }
    while(ans != num*num);

    return 0;

}
